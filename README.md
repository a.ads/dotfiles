# dotfiles

Esse é meu repositório ```dotfiles```. Espero que encontre algo útil por aqui.

## Instruções

Se tiveres logada(o) na sua conta do gitlab, [forque](https://gitlab.com/asm/dotfiles/fork/new) esse repositório.

Em seguinda, clone-o em um diretório de sua preferência (aqui irei cloná-lo no diretório ```.dotfiles```)
```
$ git clone https://gitlab.com/seuNomeDeUsuário/dotfiles.git .dotfiles
```

> Se não tiveres logada(o) em uma conta do gitlab, simplesmente clone esse repositório no diretório de sua preferência
(aqui irei cloná-lo no diretório ```.dotfiles```)
```
$ git clone https://gitlab.com/asm/dotfiles.git .dotfiles
```

Então entre no diretório,
```
$ cd .dotfiles
```
Remova o que você não usa ou não faz o menor sentido pra você, acrescente aquilo que lhe for últil, e link os arquivos
para seu diretório ```$HOME```
```
$ bash bootstrap # isso irá linkar os aquivos terminados com .symlink para o seu diretório  $HOME
```
## Como funciona?

Quaisquer arquivos terminando em *.symlink* irão ser linkados em seu diretório ```$HOME``` ao executar o script ```bootstrap```.
Dessa forma você pode manter todos eles versionados em seu diretório *"dotfiles"* (ou outro nome que vocẽ tiver escolhido), e ainda
manter aqueles arquivos autocarregados em seu diretório ```$HOME```.

## Agradecimentos

Este repositório é um fork do repositório ```dotfiles``` do
[@padawanphysicist](https://gitlab.com/padawanphysicist/dotfiles.git), a quem sou grato
por me apresentar a ideia de [repositórios dotfiles](http://zachholman.com/2010/08/dotfiles-are-meant-to-be-forked/).

## Contato

Seu tiver alguma dúvida, comentário, sugestão, crítica, ou simplesmente quiser conversar sobre a vida o universo e tudo o mais,
me escreva no endereço ```asm arroba member ponto fsf ponto org```! :smile:
