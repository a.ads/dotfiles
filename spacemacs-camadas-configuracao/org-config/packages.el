;;; packages.el --- org-config layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2016 Sylvain Benner & Contributors
;;
;; Author: asm <asm@riseup.net>
;; URL: https://gitlab.com/asm/dotfiles
;;
;; Esse arquivo não é parte do GNU Emacs.
;;
;;; Licença: GPLv3


;;; Código:

(defconst org-config-packages
  '(
    ;; Exclui pacotes fora de meu uso que estão inclusos na camada `org',
    ;; distribuída junto com o Spacemacs
    (org-pomodoro :excluded t)
    (org-bullets :excluded t)
    (org-repo-todo :excluded t)
    (org-present :excluded t)
    )
  "A lista de pacotes Lisp requerida pela camada org-config.

  Para saber mais sobre especificação de como deve ser cada entrada, consulte:
  http://spacemacs.org/doc/LAYERS.html#orgheadline11")

