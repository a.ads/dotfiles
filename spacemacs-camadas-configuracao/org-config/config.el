;;; config.el --- Org configuration File for Spacemacs
;;
;; Copyright (c) 2012-2016 Sylvain Benner & Contributors
;;
;; Author: Sylvain Benner <sylvain.benner@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; Autor: asm <asm@riseup.net>
;; URL: https://gitlab.com/asm/dotfiles
;;
;; Esse arquivo não é parte do GNU Emacs.
;;
;;; Licença: GPLv3


;; Habilita camada de configuração `org', que é distribuída
;; junto com o Spacemacs.

(configuration-layer/declare-layers '(org))
